#ifndef CEF_INCLUDE_CEF_HOST_RESOLVER_H_
#define CEF_INCLUDE_CEF_HOST_RESOLVER_H_
#pragma once

#include "include/cef_base.h"
#include "include/cef_request.h"


///
// Generic callback interface used for asynchronous completion.
///
/*--cef(source=client)--*/
class CefResolvedCompletedCallback : public virtual CefBase {
 public:
  
  /*--cef()--*/
  virtual void OnResolveCompleted(int result) =0;

  /*--cef()--*/
  virtual void OnAddressesResolved(const std::vector<CefString>& resolved_ips) =0;
};


/*--cef(source=library)--*/
class CefHostResolver : public virtual CefBase {
 public:
  /*--cef()--*/
  static int ResolveFromCache(CefRefPtr<CefRequest> request, 
                              std::vector<CefString>& resolved_ips);

  /*--cef()--*/
  static int Resolve(CefRefPtr<CefRequest> request, 
                     CefRefPtr<CefResolvedCompletedCallback> completion_callback);

  /*--cef()--*/
  virtual void OnResolveCompleted(int result) =0;
};

#endif  // CEF_INCLUDE_CEF_HOST_RESOLVER_H_