#include "host_resolver_impl.h"

#include "libcef/browser/content_browser_client.h"
#include "libcef/utility/cef_net_log.h"

#include "base/bind.h"
#include "base/bind_helpers.h"
#include "net/base/net_errors.h"
#include "url/gurl.h"

//static
void CreateResolveRequestParams(CefRefPtr<CefRequest> input_request,
                                net::HostResolver::RequestInfo* out_request_info,
                                net::BoundNetLog* out_bound_net_log) {
    GURL request_url(input_request->GetURL().ToString());
    net::HostPortPair host_port_pair(request_url.host(), request_url.EffectiveIntPort());
    out_request_info->set_host_port_pair(host_port_pair);
    *out_bound_net_log = net::BoundNetLog::Make(CefNetLog::Get(), net::NetLog::SOURCE_URL_REQUEST);
}

//static
int CefHostResolver::ResolveFromCache(CefRefPtr<CefRequest> request, 
                                      std::vector<CefString>& addresses) {
  int return_value = net::ERR_FAILED;
  CefContentBrowserClient* cef_content_browser_client = CefContentBrowserClient::Get();

  if (cef_content_browser_client != NULL) {
    scoped_refptr<CefBrowserContextImpl> browser_context = cef_content_browser_client->browser_context();
    if (browser_context) {
      scoped_refptr<CefURLRequestContextGetterImpl> request_context = browser_context->request_context();
      if (request_context) {
        net::HostResolver* host_resolver = request_context->GetHostResolver();
  
        if (host_resolver) {
          net::HostPortPair host_port_pair;
          net::HostResolver::RequestInfo request_info(host_port_pair);
          net::BoundNetLog bound_net_log;
          CreateResolveRequestParams(request, &request_info, &bound_net_log);

          net::AddressList address_list;
          return_value = host_resolver->ResolveFromCache(request_info, &address_list, bound_net_log);

          if (return_value == net::OK) {
            for (net::AddressList::const_iterator iter = address_list.begin(); iter != address_list.end(); ++iter) {
              addresses.push_back(CefString(iter->ToString()));
            }
          }
        }
      }
    }
  }

  return return_value;
}

//static
int CefHostResolver::Resolve(CefRefPtr<CefRequest> request, 
                             CefRefPtr<CefResolvedCompletedCallback> resolved_completed_callback_obj) {

  int return_value = net::ERR_FAILED;
  CefContentBrowserClient* cef_content_browser_client = CefContentBrowserClient::Get();

  if (cef_content_browser_client != NULL) {
    scoped_refptr<CefBrowserContextImpl> browser_context = cef_content_browser_client->browser_context();
    if (browser_context) {
      scoped_refptr<CefURLRequestContextGetterImpl> request_context = browser_context->request_context();
      if (request_context) {
        net::HostResolver* host_resolver = request_context->GetHostResolver();
        if (host_resolver) {
          net::HostPortPair host_port_pair;
          net::HostResolver::RequestInfo request_info(host_port_pair);
          net::BoundNetLog bound_net_log;
          CreateResolveRequestParams(request, &request_info, &bound_net_log);
       
          CefRefPtr<CefHostResolverImpl> cef_host_resolver_impl = new CefHostResolverImpl(request, resolved_completed_callback_obj);

          return_value = host_resolver->Resolve(request_info, net::DEFAULT_PRIORITY, cef_host_resolver_impl->GetAddressList(), 
                  base::Bind(&CefHostResolverImpl::OnResolveCompleted, cef_host_resolver_impl.get()), NULL, bound_net_log);

          if (return_value == net::OK) {
              cef_host_resolver_impl->NotifyResolvedAddresses();
          } else if (return_value == net::ERR_IO_PENDING) {
              cef_host_resolver_impl->AddRef();
          }
        }
      }
    }
  }

  return return_value;
}

CefHostResolverImpl::CefHostResolverImpl(CefRefPtr<CefRequest> request, 
                          				       CefRefPtr<CefResolvedCompletedCallback> resolved_completed_callback_obj) 
												                        :	m_request(request), 
												                        	m_resolved_completed_callback_obj(resolved_completed_callback_obj),
												                        	m_address_list()
{
}

CefHostResolverImpl::~CefHostResolverImpl()
{
}

void CefHostResolverImpl::OnResolveCompleted(int result)
{
  if (result == net::OK) {
    NotifyResolvedAddresses();
  }
	m_resolved_completed_callback_obj->OnResolveCompleted(result);
	Release();
}

net::AddressList* CefHostResolverImpl::GetAddressList()
{
	return &m_address_list;
}

void CefHostResolverImpl::NotifyResolvedAddresses() 
{
    std::vector<CefString> resolved_addresses;
    GetResolvedAddresses(resolved_addresses);
    m_resolved_completed_callback_obj->OnAddressesResolved(resolved_addresses);
}

void CefHostResolverImpl::GetResolvedAddresses(std::vector<CefString>& resolved_addresses)
{
    for (net::AddressList::const_iterator iter = m_address_list.begin(); iter != m_address_list.end(); ++iter) {
      resolved_addresses.push_back(iter->ToStringWithoutPort());
    }
}