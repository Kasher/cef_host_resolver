#ifndef CEF_LIBCEF_HOST_RESOLVER_H_
#define CEF_LIBCEF_HOST_RESOLVER_H_
#pragma once

#include <vector>
#include "include/cef_host_resolver.h"
#include "net/base/address_list.h"
#include "net/dns/host_resolver.h"

class CefHostResolverImpl : public CefHostResolver {
  public:
      CefHostResolverImpl(CefRefPtr<CefRequest> request, 
                          CefRefPtr<CefResolvedCompletedCallback> resolved_completed_callback_obj);
      virtual ~CefHostResolverImpl();

      virtual net::AddressList* GetAddressList();

      virtual void OnResolveCompleted(int result);
  
      virtual void NotifyResolvedAddresses();
  
    private:
      virtual void GetResolvedAddresses(std::vector<CefString>& resolved_addresses);

      CefRefPtr<CefRequest>                         m_request;
      CefRefPtr<CefResolvedCompletedCallback>       m_resolved_completed_callback_obj;
      net::AddressList                              m_address_list;

    // Include the default reference counting implementation.
    IMPLEMENT_REFCOUNTING(CefHostResolverImpl);
};

#endif  // CEF_LIBCEF_HOST_RESOLVER_H_